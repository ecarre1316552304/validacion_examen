const codigo = document.getElementById("codigo");
const marca = document.getElementById("marca");
const modelo = document.getElementById("modelo");
const año = document.getElementById("año");
const feinicial = document.getElementById("feinicial");
const fefinal = document.getElementById("fefinal");
const form = document.getElementById("form");
const parrafo = document.getElementById("warnings");

form.addEventListener("submit", e=>{
    let warnings = ""
    let entrar = false 
    parrafo.innerHTML = ""
    
    if(codigo.value.length !=5){
        alert(`El usuario codigo ingresado no es valido. Codigo de 5 caracteres necesario.`) ;
    }
    if(marca.value.length !=50){
        alert(`La marca ingresada no es valido. Marca de 50 caracteres necesario.`) ;
    }
    if(modelo.value.length !=30){
        alert(`El modelo ingresado no es valido. Modelo de 30 caracteres necesario.`) ;
    }
    if(año.value.length !=4){
        alert(`El año ingresado no es valido. Ingrese año valido de 4 caracteres`) ;
    }
    if (form.feinicial.value==0){
        alert('El campo de fecha inicial esta vacio, ingrese una fecha');
        form.feinicial.value="";
        form.feinicial.focus;
        return false;
    }
    if (form.fefinal.value==0){
        alert('El campo de fecha final esta vacio, ingrese una fecha');
        form.fefinal.value="";
        form.fechafinal.focus;
        return false;
    }
    if (Date.parse(fefinal) < Date.parse(feinicial)) {
        alert(` La fecha final ingresada no es valida. Fecha final debe ser mayor a la inicial.`) ;
        form.fefinal.value="";
        form.fefinal.focus();
        entrar = true
    }
    if(entrar){
        parrafo.innerHTML = warnings
        e.preventDefault()
    }
    else  {
        alert(`Bienvenido`); 
     }
})